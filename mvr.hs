{-|
  mvr.hs    Move Recursively

  Usage:    mvr FILE [FILE ...]

  Examples: mvr hugo
            mvr /path/to/hugo
            mvr hugo egon lisa fritz
            mvr -h

  Compile:  ghc --make -Wall mvr.hs
  Lint:     hlint -s mvr.hs

  Author:   haskell@wu6ch.de
-}


import Control.Monad (mzero)
import Control.Monad.Trans (liftIO)
import Control.Monad.Trans.Maybe (MaybeT, runMaybeT)

import Data.Bool (bool)
import Data.List (isPrefixOf)

import System.Directory (doesFileExist, renameFile)
import System.Environment (getArgs)
import System.FilePath ((</>), takeDirectory, takeFileName)


{-|
  The character that is added in front of a file name to increase the backup
  level.
-}
levelChar :: Char
levelChar = '_'


{-|
  The maximum number of backup levels.
-}
maxLevel :: Int
maxLevel = 8


{-|
  Takes a FilePath and increases the backup level by adding a levelChar in front
  of the file name.

  >>> addLevel "/path/to/hugo"
  "/path/to/_hugo"

  >>> addLevel "/path/to/_hugo"
  "/path/to/__hugo"
-}
addLevel :: FilePath -> FilePath
addLevel filePath = dirName </> (levelChar : fileName)
  where
    dirName  = takeDirectory filePath
    fileName = takeFileName  filePath


{-|
  Takes a FilePath and returns True, if maxLevel is reached.

  >>> reachedLimit "/path/to/_______hugo"
  False

  >>> reachedLimit "/path/to/________hugo"
  True
-}
reachedLimit :: FilePath -> Bool
reachedLimit filePath = replicate maxLevel levelChar `isPrefixOf` fileName
  where
    fileName = takeFileName filePath


{-|
  Takes a FilePath and returns the FilePath of the first existing backup level
  (in MaybeT IO context).

  $ touch ___hugo

  >>> runMaybeT $ getFilePath "hugo"
  Just "./___hugo"

  >>> runMaybeT $ getFilePath "egon"
  Nothing
-}
getFilePath :: FilePath -> MaybeT IO FilePath
getFilePath filePath = do
  exists <- liftIO $ doesFileExist filePath
  if exists
    then return filePath
    else bool (getFilePath $ addLevel filePath) mzero (reachedLimit filePath)


{-|
  Takes a FilePath and checks, if a file with this name, but with the next
  backup level) is available. (The function is called recursively in this case.)
  The function then renames the given file path to the next backup level.

  $ touch ___hugo

  >>> runMaybeT $ recRename "___hugo"
  Just ()

  $ ls -1 ____hugo
  ____hugo
-}
recRename :: FilePath -> MaybeT IO ()
recRename filePath = do
  let newPath = addLevel filePath
  exists <- liftIO $ doesFileExist newPath
  bool (return ()) (recRename newPath) exists
  liftIO $ renameFile filePath newPath


{-|
  Takes a FilePath and runs the Maybe monad transformer on the combination of
  'getFilePath' and 'recRename'. Outputs a related error message, if the file is
  not available.

  $ touch ___hugo

  >>> recMove "hugo"

  $ ls -1 ____hugo
  ____hugo

  >>> recMove "egon"
  'egon' not found!
-}
recMove :: FilePath -> IO ()
recMove filePath = do
  result <- runMaybeT $ getFilePath filePath >>= recRename
  case result of
    Nothing -> putStrLn $ mconcat ["'", filePath, "' not found!"]
    _       -> return ()


{-|
  'main': Evaluates the command line arguments and applies 'recMove' on each
  command line argument, or outputs a usage, if '-h' is given.
-}
main :: IO ()
main = do
  let usage = "Usage: mvr FILE [FILE ...]"
  args <- getArgs
  bool (mapM_ recMove args) (putStrLn usage) ("-h" `elem` args)


-- EOF
