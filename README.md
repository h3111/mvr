# _mvr_ - _Move Recursively_


## Rationale

Unless using Git anyway, I like to backup a file before applying modifications.
Instead of copying the file to another location or renaming it by hand, I use
the following "recipe":

* Get the file, to be handled: `hugo`.
* Add one underscore in front of the file name: `_hugo`.
* If there is already a file available with this name, add one underscore to
  this name: `__hugo`.
* Repeat recursively, if necessary.

As a consequence, there might be a file list like this after a while:

````bash
$ ls -1 *hugo*
hugo      # backup level 0
_hugo     # backup level 1
__hugo    # backup level 2
___hugo   # backup level 3
____hugo  # backup level 4
````

In this list, `hugo` is the up-to-date version (with backup level 0). The more
underscores a file name has (i.e. the higher the backup level is), the older the
file version is.


## The `mvr` tool

Instead of renaming and adding underscores by hand, I use the tool `mvr`
(_move recursively_), which is written in Haskell. It "lifts" the backup levels
of all files with a given file name by one step recursively.


### Compiling and Linting

Since `mvr.hs` does not use "special" modules, the compilation should be
possible with the standard Haskell installation:

````bash
$ sudo apt install haskell-platform hlint
$ ghc --make -Wall mvr.hs
````

Lint:

````bash
$ hlint -s mvr.hs
````

### "Installation"

Copy `mvr` to a directory that is listed in the environment variable `PATH`,
e.g. `~/bin` (or create a symlink).


### Usage

Call `mvr` this way to get a short usage message:

````bash
$ mvr -h
Usage: mvr FILE [FILE ...]
````

Example:

````bash
$ echo "1" > hugo
$ mvr hugo
$ echo "2" > hugo
$ mvr hugo
$ echo "3" > hugo

$ ls -1 *hugo*
hugo
_hugo
__hugo

$ cat hugo _hugo __hugo
3
2
1

$ mvr hugo
$ echo "4" > hugo

$ ls -1 *hugo*
hugo
_hugo
__hugo
___hugo

$ cat hugo _hugo __hugo ___hugo
4
3
2
1
````


## Additional hints

It is possible to enter more than one file name as command line parameter; all
files are handled independently. Example:

````bash
$ mvr hugo egon lisa fritz
````

A complete (relative or absolute) file path is also allowed. Example:

````bash
$ mvr /abs/path/to/hugo rel/path/to/egon
````

If the file with the name, given as command line parameter, is not available,
`mvr` checks recursively, if the file with up to 8 underscores in front of the
file name exists. This way, you do not need to count underscores. Example:

````bash
$ echo "2" > __hugo
$ echo "3" > ___hugo

$ ls -1 *hugo*
__hugo
___hugo

$ cat __hugo ___hugo
2
3

$ mvr hugo

$ ls -1 *hugo*
___hugo
____hugo

$ cat ___hugo ____hugo
2
3
````


<sub>[Wolfgang](mailto:haskell@wu6ch.de)</sub>


<!-- EOF -->
